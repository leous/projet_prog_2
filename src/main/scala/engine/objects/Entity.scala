package engine

import sfml.system.*
import sfml.graphics.*
import sfml.window.*
import scenes.*
import main.*

import java.io.File
import scala.collection.View.FlatMap
import scala.collection.mutable.ListBuffer
import scala.collection.mutable.ArrayBuffer

// Les entités seront affichées à l'écran, mais on ne souhaite pas charger la texture
// de l'entité a chaque fois, car c'est une opération couteuse.
// On les charges une fois et on les références dans une map.

//Table permettant d'accéder à la texture d'une entity
object EntityTable {
  var entityTable: Map[String, EntityTexture] = Map()
  val path: String = "src/main/resources/entities/"
  val fileArray: Array[String] = File(path).list()
  def load() = for (t <- fileArray) EntityTexture(t).load()
}

//Charge chaque texture d'entity
class EntityTexture(file: String) extends GameObject {
  val texture: Texture = Texture()
  override def load() =
    texture.loadFromFile(EntityTable.path + file)
    EntityTable.entityTable = EntityTable.entityTable + (file -> this)
}

enum Alignement:
  case Neutral, Ally, Enemy

trait Health {
  this: Entity =>
  var maxHealth: Int = 1
  var health: Int = maxHealth
  def dies(): Unit = this.removeThis()
}

trait Mobile {}

trait Combative {
  this: Entity =>
  var inCombat: Boolean = false
  var fightTarget: Entity = null
  var combatPosition: Double = 0
  var attackInterval: Int = 0
  var damage: Int = 0
  def attack(): Unit = ()
}

//objet gérant les éléments principaux de la scène.
class Entity(file: String) extends GameObject {

  var place: GamePlayScene = null

  var position: Vector2[Float] = Vector2(0, 0)
  var velocity: Vector2[Float] = Vector2(0, 0)
  val speed: Float = 0
  var isMoving: Boolean = false

  val size: Vector2[Float] = Vector2(0, 0)
  var hitbox: Hitbox = {
    var h = Hitbox()
    h.parent = this
    h
  }

  var destination: Vector2[Float] | Entity | Enemy = _
  var selected: Boolean = false

  // cost to spawn entity
  val soulCost: Int = 0
  val stoneCost: Int = 0

  val alignement: Alignement = Alignement.Neutral
  def isNeutral(): Boolean = this.alignement == Alignement.Neutral
  def isAlly(): Boolean = this.alignement == Alignement.Ally
  def isEnemy(): Boolean = this.alignement == Alignement.Enemy

  var sprite: Sprite = Sprite(EntityTable.entityTable(file).texture)

  var isGrouped = false
  var group: EntityGroup = null

  override def load() =
    super.load()
    sprite.origin =
      Vector2(sprite.globalBounds.width / 2, sprite.globalBounds.height / 2)
    sprite.position = this.position

  override def draw(): Unit =
    this.sprite.position = this.position
    window.draw(this.sprite)
}

class EntityGroup extends GameObject {

  var entities: ArrayBuffer[Entity] = ArrayBuffer()

  def addEntity(ent: Entity) =
    entities += ent
    ent.group = this
    ent.isGrouped = true

  def removeEntity(ent: Entity) =
    entities -= ent
    ent.group = null
    ent.isGrouped = false

  var position: Vector2[Float] = Vector2(0, 0)
  var destination: Vector2[Float] = Vector2(0, 0)
  var isMoving: Boolean = false
  var startMoving = false
  val speed: Float = 0
  var selected: Boolean = false

}
