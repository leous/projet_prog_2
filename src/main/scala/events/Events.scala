package events

import sfml.window.Mouse.Button.*
import sfml.window.Keyboard.Key.*
import sfml.window.Mouse.*
import sfml.window.Keyboard.*
import sfml.window.*
import sfml.system.*
import main.*
import scenes.*
import gameLoop.*

object Events {

  // donne la position de la souris dans la scene principale de l'etat sur le dessus de la pile
  def mousePos =
    window.mapPixelToCoords(
      Mouse.position(window),
      StateStack.stack.head.mainScene.view
    )

  def trueMousePos =
    Vector2(Mouse.position(window).x.toFloat, Mouse.position(window).x.toFloat)

  // gere l'ensemble des evenements souhaités
  def handleWindowEvents() =
    for event <- window.pollEvent() do
      event match
        case Event.Closed()      => window.close()
        case Event.LostFocus()   => isPressed += (KeyEscape -> true)
        case Event.GainedFocus() => isPressed += (KeyEscape -> false)
        case Event.Resized(_, _) => isResized = true
        case Event.KeyPressed(_, _, _, _, _) |
            Event.KeyReleased(_, _, _, _, _) |
            Event.MouseButtonPressed(_, _, _) |
            Event.MouseButtonReleased(_, _, _) =>
          update(event)
        case _ => ()

  // map des keys / buttons préssés
  val defaultMap: Map[Key | Button, Boolean] =
    Map(
      KeyNum1 -> false,
      KeyNum2 -> false,
      KeyNum3 -> false,
      KeyQuote -> false,
      KeyNum5 -> false,
      KeyHyphen -> false,
      KeyNum7 -> false,
      KeyZ -> false,
      KeyQ -> false,
      KeyS -> false,
      KeyD -> false,
      KeyG -> false,
      KeyC -> false,
      KeyV -> false,
      KeyUp -> false,
      KeyDown -> false,
      KeyLeft -> false,
      KeyRight -> false,
      KeyLShift -> false,
      KeyLControl -> false,
      KeyEscape -> false,
      KeyEnter -> false,
      KeyBackspace -> false,
      Left -> false,
      Right -> false
    )

  var isPressed = defaultMap

  var isReleased = defaultMap

  var isResized: Boolean = false

  def reset(): Unit = isReleased = defaultMap

  // met a jour la map "isPressed"
  def update(event: Event): Unit =
    event match
      case Event.KeyPressed(key, _, _, _, _) =>
        isPressed += (key -> true)
      case Event.KeyReleased(key, _, _, _, _) =>
        isReleased += (key -> true)
        isPressed += (key -> false)
      case Event.MouseButtonPressed(key, _, _) =>
        isPressed += (key -> true)
      case Event.MouseButtonReleased(key, _, _) =>
        isReleased += (key -> true)
        isPressed += (key -> false)
      case _ => ()

}
