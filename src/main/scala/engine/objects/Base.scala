package engine

import events.*
import sfml.window.*
import sfml.system.*
import main.*

class Base(file: String = "base.png") extends Entity(file: String) with Health {

  override val size = Vector2(64, 64)
  override val alignement = Alignement.Neutral
  override val stoneCost = 20

  var wasSelected = false

  def collect[R <: GroundRessource](res: R): Unit =
    res match
      case s: GroundStone => res.ressource.add(res.weight - 1)
      case _              => res.ressource.add(res.weight)
    res.detachAllGolems()
    res.removeThis()

  override def loop(): Unit =
    super.loop()
    if Events.isPressed(Mouse.Button.Right) then
      if isOver(this, Events.mousePos) then wasSelected = true
    else
      if wasSelected then
        this.place.spawnEntity(
          Golem(),
          this.position.x - 50 + random.nextInt(100),
          this.position.y - 50 + random.nextInt(100)
        )
      wasSelected = false

  override def load(): Unit =
    super.load()
    addComponent(HealthManagementBase(this))
    maxHealth = 10
    health = maxHealth
    addComponent(TargetSelect(this))

}
