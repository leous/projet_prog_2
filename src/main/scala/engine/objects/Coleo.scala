package engine

import engine.*
import sfml.system.Vector2
import main.*

//class DirtBall extends GameObjects with Mobile with Combative {
//  override sprite
//}

class DirtBall(file: String = "dirtball.png") extends Entity(file: String) {

  override val size: Vector2[Float] = Vector2(64, 64)
  override val speed = 8
  override val alignement: Alignement = Alignement.Neutral

  var dir: Vector2[Float] = Vector2(0, 0)

  var thrower: Coleo = null
  val damage: Int = 2

  override def load(): Unit =
    super.load()
    addComponent(DirtBallMovement(this))
    isMoving = true

  override def loop(): Unit =
    // supprime la boule si elle sort du terrain
    if (dist(this.position, thrower.position) > 300) then removeThis()
    if !(this.sprite.globalBounds.intersects(this.place.sprite.globalBounds))
    then removeThis()

}

class Coleo(file: String = "coleo.png") extends Enemy(file: String) {

  var combatAnimationStep = 0

  override def load() =
    super.load()
    maxHealth = 10
    health = maxHealth
    attackInterval = 5
    damage = 0

  override def draw() =
    // very not pretty but there is only two animation to see when the enemy attack so...
    if combatAnimationStep > 0 then
      combatAnimationStep match
        case 1   => this.sprite.rotate(30)
        case 31  => this.sprite.rotate(30)
        case 61  => this.sprite.rotate(30)
        case 91  => this.sprite.rotate(30)
        case 121 => this.sprite.rotate(30)
        case 151 => this.sprite.rotate(30)
        case 181 => this.sprite.rotate(30)
        case 211 => {
          this.sprite.rotate(random.nextInt(60)); combatAnimationStep = -1
        }
        case _ => ()
      combatAnimationStep += 1
    super.draw()

  def shootBall(angle: Int): Unit =
    var shootAngle = angle * scala.math.Pi.toFloat / 180.toFloat
    var dirtBall = DirtBall()
    var dir = Vector2(
      scala.math.cos(shootAngle).toFloat,
      scala.math.sin(shootAngle).toFloat
    )
    dirtBall.destination = this.position + dir * 2.toFloat
    dirtBall.thrower = this
    this.place.addEntity(
      dirtBall,
      this.position.x + dir.x,
      this.position.y + dir.y
    )

  override def attack(): Unit =
    combatAnimationStep = 1
    var shootAngle = random.nextInt(360)
    shootBall(shootAngle)
    shootBall(shootAngle + 120)
    shootBall(shootAngle - 120)

}
