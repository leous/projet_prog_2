package gameLoop

import main.*
import scenes.*
import sfml.graphics.*
import scala.collection.mutable.Stack

//pile des etats du jeu
// - seul l'etat sur le dessus de la pile est mis a jour (updateSate et loopState)
// - les etats sont dessiner depuis le fond de la pile, du dernier etat non transparent jusqu'au dessus de la pile
object StateStack {

  var stack: List[GameState] = List()

  def push(state: GameState): Unit =
    state.load()
    stack = state :: stack

  def pop(): Unit = stack = stack.tail

  def drawStack(stateList: List[GameState] = stack): Unit = stateList match
    case List() => ()
    case (state: GameState) :: q =>
      (if state.isTransparent then drawStack(q)); state.drawState()

}

//un etat du jeu est caractériser par une scène initiale "mainScene"
class GameState(initialScene: Scene) {

  val mainScene: Scene =
    initialScene // scene principale du jeu, toujours au fond de la pile de scenes

  def load(): Unit =
    if !mainScene.loaded then mainScene.load()

  var layers: Stack[Scene] =
    Stack().push(
      mainScene
    ) // pile de scenes, les scenes sont toutes mise a jour et dessinée depuis le fond de la pile

  var isTransparent: Boolean =
    false // si un etat est transparent, les états en dessous dans la pile d'etat sont déssiné

  def loopState(): Unit =
    for (layer <- layers.reverseIterator)
      gameLoop.loop(layer)

  def drawState(): Unit =
    for (layer <- layers.reverseIterator)
      gameLoop.draw(layer)

  def updateState(): Unit =
    () // Une scene peut override cette methode pour intéragir avec d'autres états par exemple

}
