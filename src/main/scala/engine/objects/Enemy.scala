package engine

import engine.*
import sfml.system.Vector2
import main.*

// Exemple d'enemi. Les Golems seront rapidement nombreux dans le gameplay,
// tandis que les ennemis seront plus costaud (plus de vie) mais peu nombreux.
// Il faudra alors essayer de fabriquer un maximum de golem
class Enemy(file: String)
    extends Entity(file: String)
    with Health
    with Mobile
    with Combative {

  override val size: Vector2[Float] = Vector2(128, 128)
  override val alignement: Alignement = Alignement.Enemy

  override def dies(): Unit =
    super.dies()
    this.place.addEntity(
      GroundSoul(),
      this.position.x + 30 - random.nextInt(60),
      this.position.y + 30 - random.nextInt(60)
    )

  override def load() =
    super.load()
    addComponent(Movement(this))
    addComponent(TargetSelect(this))
    addComponent(EnemyCombat(this))
    addComponent(HealthManagement(this))
    this.sprite.rotate(random.nextInt(360))

  override def loop() =
    var angryGolemFound = false
    for (o <- this.parent.subObjects) do
      if !angryGolemFound then
        o match
          case g: Golem =>
            if g.inCombat && g.fightTarget == this then angryGolemFound = true
          case _ => ()
    this.inCombat = angryGolemFound

}
