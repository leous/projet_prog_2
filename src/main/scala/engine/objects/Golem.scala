package engine

import sfml.system.*

// Unité principale du jeu.
class Golem(file: String = "golem.png")
    extends Entity(file: String)
    with Health
    with Mobile
    with Combative {

  override val size: Vector2[Float] = Vector2(64, 64)
  override val speed = 5
  override val alignement: Alignement = Alignement.Ally

  override val stoneCost: Int = 1
  override val soulCost: Int = 1

  val carryForce: Int = 1

  override def load() =
    super.load()
    addComponent(GolemMovement(this))
    addComponent(AllySelect(this))
    addComponent(AllyCombat(this))
    addComponent(HealthManagement(this))
    attackInterval = 1
    damage = 1

  var isAttached: Boolean = false
  var attachement: GroundRessource = null

  def detach() =
    isAttached = false
    attachement.attachedGolems = attachement.attachedGolems.filter(_ != this)
    attachement = null

}
