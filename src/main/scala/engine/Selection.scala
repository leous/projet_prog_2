package selection

import scala.collection.mutable.ListBuffer
import main.window
import sfml.system.*
import sfml.graphics.*
import engine.*
import events.*
import sfml.window.Mouse.Button.Left
import sfml.window.Keyboard.Key.KeyLShift

object Selection extends GameObject {

  var zoneDisplay: RectangleShape = {
    var zone = RectangleShape()
    zone.fillColor = Color.apply(255, 255, 255, 150)
    zone.outlineThickness = 5.toFloat
    zone.outlineColor = Color.apply(255, 255, 255, 240)
    zone
  }

  var isLongSelecting: Boolean = false
  var isSelecting: Boolean = false
  var wasLongSelecting: Boolean = false
  var wasSelecting: Boolean = false
  var firstClicPos: Vector2[Float] = _

  def zoneSelected: Rect[Float] = zoneDisplay.globalBounds

  var selectedEntities: ListBuffer[Entity] = ListBuffer()

  def addSelection(obj: Entity) =
    obj.selected = true
    selectedEntities += obj

  def target(dest: Vector2[Float] | Entity) =
    for (obj <- selectedEntities) do
      obj match
        case obj: Golem => {
          if obj.isAttached then
            if obj.attachement.canBeCarried(selectedEntities) then
              obj.attachement.detachGolemsExcept(selectedEntities)
            else obj.attachement.detachGolemsOnly(selectedEntities)
            dest match
              case (point: Vector2[Float]) => ()
              case (base: Base)            => ()
              case _ => if obj.isAttached then obj.detach()
        }
        case _ => ()
      obj.destination = dest
      obj.isMoving = true
      obj.selected = false
    selectedEntities = ListBuffer()

  override def loop(): Unit =
    if Events.isPressed(Left) then
      // Long Selecting -----
      if Events.isPressed(KeyLShift) then
        if this.isLongSelecting then
          this.wasLongSelecting = true
          zoneDisplay.size = Events.mousePos - firstClicPos
        else
          this.isLongSelecting = true
          this.isSelecting = false
          this.wasSelecting = false
          this.wasLongSelecting = false

          firstClicPos = Events.mousePos
          zoneDisplay.size = Vector2(0, 0)
          zoneDisplay.position = firstClicPos

      // Short Selecting -----
      else if this.isSelecting then this.wasSelecting = true
      else if !this.isLongSelecting then
        this.isSelecting = true
        firstClicPos = Events.mousePos
        this.isLongSelecting = false

    // Not Selecting
    else
      if !this.isSelecting then this.wasSelecting = false
      if !this.isLongSelecting then this.wasLongSelecting = false
      this.isSelecting = false
      this.isLongSelecting = false

  override def draw(): Unit =
    if this.isLongSelecting then window.draw(zoneDisplay)
}
