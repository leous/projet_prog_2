package engine

import sfml.graphics.Text
import sfml.system.Vector2
import main.font
import main.window
import sfml.graphics.Color
import sfml.graphics.RectangleShape

//Gère la santé des unités. Sujet à changements.
class HealthManagement(obj: Entity with Health)
    extends Component[Entity](obj: Entity) {

  var healthBar: RectangleShape = {
    var bar = RectangleShape()
    bar.size = Vector2(100, 7)
    bar.origin =
      Vector2(bar.globalBounds.width / 2, bar.globalBounds.height / 2)
    bar.outlineColor = Color.Black()
    bar.outlineThickness = 2.toFloat
    bar.fillColor = Color.Black()
    bar
  }

  var healthDisplay: RectangleShape = {
    var bar = RectangleShape()
    bar.size = Vector2(100, 7)
    bar.origin =
      Vector2(bar.globalBounds.width / 2, bar.globalBounds.height / 2)
    bar.outlineColor = Color.Transparent()
    bar.fillColor = Color.Green()
    bar
  }

  override def loop() =
    if obj.health <= 0 then obj.dies()
    else
      healthDisplay.size = Vector2(100 * obj.health / obj.maxHealth, 10)
      if obj.health < obj.maxHealth.toFloat / 3.toFloat then
        healthDisplay.fillColor = Color.Red()
      else if obj.health < 2 * obj.maxHealth.toFloat / 3.toFloat then
        healthDisplay.fillColor = Color.Yellow()

  override def draw() =
    obj match
      case g: Golem => ()
      case _ =>
        healthBar.position = obj.position + Vector2(0, -2 * obj.size.y / 3)
        healthDisplay.position = obj.position + Vector2(0, -2 * obj.size.y / 3)
        window.draw(healthBar)
        window.draw(healthDisplay)

}

class HealthManagementBase(obj: Base)
    extends HealthManagement(obj: Entity with Health) {

  override def load(): Unit =
    healthDisplay = {
      var bar = RectangleShape()
      bar.size = Vector2(64, 64)
      bar.origin =
        Vector2(bar.globalBounds.width / 2, bar.globalBounds.height / 2)
      bar.outlineThickness = 5.toFloat
      bar.outlineColor = Color.Green()
      bar.fillColor = Color.Transparent()
      bar
    }

  override def loop() =
    if obj.health <= 0 then obj.dies()
    else
      healthBar.position = obj.position + Vector2(0, -2 * obj.size.y / 3)
      if obj.health < obj.maxHealth.toFloat / 3.toFloat then
        healthDisplay.outlineColor =
          Color.Red() // difference with normal healthManagement
      else if obj.health < 2 * obj.maxHealth.toFloat / 3.toFloat then
        healthDisplay.outlineColor =
          Color.Yellow() // difference with normal healthManagement

  override def draw() =
    healthDisplay.position = obj.position
    window.draw(healthDisplay)

}
