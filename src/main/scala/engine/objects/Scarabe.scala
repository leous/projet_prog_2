package engine

import engine.*
import sfml.system.Vector2
import main.*

class Scarabe(file: String = "scarabe.png") extends Enemy(file: String) {

  var combatAnimationStep = 0

  override def load() =
    super.load()
    maxHealth = 7
    health = maxHealth
    attackInterval = 5
    damage = 1

  override def draw() =
    // very not pretty but there is only two animation to see when the enemy attack so...
    if combatAnimationStep > 0 then
      combatAnimationStep match
        case 1  => this.sprite.rotate(-30)
        case 11 => this.sprite.rotate(30)
        case 21 => this.sprite.rotate(30)
        case 31 => this.sprite.rotate(30)
        case 41 => this.sprite.rotate(-30)
        case 51 => this.sprite.rotate(-30)
        case 61 => this.sprite.rotate(random.nextInt(30))
        case 71 => {
          this.sprite.rotate(-random.nextInt(30)); combatAnimationStep = -1
        }
        case _ => ()
      combatAnimationStep += 1
    super.draw()

  def hitNearbyAllies(): Unit =
    var hasHit = false
    for (o <- this.parent.subObjects) do
      o match
        case o: Golem =>
          if this.hitbox.intersects(o.hitbox) && !hasHit then
            o.health = o.health - this.damage
            hasHit = true
        case _ => ()

  override def attack(): Unit =
    hitNearbyAllies()
    combatAnimationStep = 1

}
