package engine

import sfml.system.*
import sfml.graphics.*
import scala.math.*

//basic functions on vector

class Hitbox {

  var parent: Entity = null

  def contains(pos: Vector2[Float]): Boolean =
    dist(parent.position, pos) < this.parent.size.x / 2

  def intersects(hitbox: Hitbox): Boolean =
    dist(
      parent.position,
      hitbox.parent.position
    ) < this.parent.size.x / 2 + hitbox.parent.size.x / 2

  def nearby(pos: Vector2[Float], space: Float): Boolean =
    dist(parent.position, pos) < this.parent.size.x / 2 + space

  def nearbyHitbox(hitbox: Hitbox, space: Float): Boolean =
    dist(
      parent.position,
      hitbox.parent.position
    ) < this.parent.size.x / 2 + hitbox.parent.size.x / 2 + space

}

def dist(vect1: Vector2[Float], vect2: Vector2[Float]): Float =
  sqrt(pow(vect1.x - vect2.x, 2) + pow(vect1.y - vect2.y, 2)).toFloat

def norm(vect: Vector2[Float]): Float =
  sqrt(pow(vect.x, 2) + pow(vect.y, 2)).toFloat

def normalize(vect: Vector2[Float]): Vector2[Float] =
  vect * (1 / norm(vect))

def centerRotate(e: Entity, teta: Float): Unit = // work on square sprites
  var oldCenterX = e.size.x / 2
  var oldCenterY = -e.size.y / 2
  var tetaRad = teta * Pi / 180
  var cosTeta = cos(tetaRad)
  var sinTeta = sin(tetaRad)
  var newCenterX = cosTeta * oldCenterX + sinTeta * oldCenterY
  var newCenterY = cosTeta * oldCenterY + sinTeta * oldCenterX
  e.sprite.rotate(teta)
  e.sprite.move(100.toFloat, 100.toFloat)
  // e.sprite.move(oldCenterX.toFloat - newCenterX.toFloat, oldCenterY.toFloat - newCenterY.toFloat)
