package gameLoop

import main.*
import events.*
import sfml.window.Keyboard.Key.*
import scenes.*

object MenuPauseGameState extends GameState(MenuPauseScene) {

  this.isTransparent = true

  override def updateState() =
    if Events.isPressed(KeyEnter) then StateStack.pop()

}

object InsectsWinState extends GameState(InsectsWinScene) {
  this.isTransparent = true
}

object GolemsWinState extends GameState(GolemsWinScene) {
  this.isTransparent = true
}
