package gameLoop

import main.*
import events.*
import sfml.window.Keyboard.Key.*
import scenes.*
import engine.*

class GamePlayGameState(initialScene: Scene)
    extends GameState(initialScene: Scene) {

  override def load() =
    super.load()
    if !Inventory.loaded then Inventory.load()
    this.layers.push(Inventory)

  override def updateState() =
    if Events.isPressed(KeyEscape) then StateStack.push(MenuPauseGameState)
    if Events.isReleased(KeyG) then
      StateStack.pop()
      StateStack.push(EditorState(this.mainScene))

    // win conditions
    var hasGolem = false
    var hasInsect = false
    for (o <- mainScene.subObjects) do
      o match
        case _: Golem => hasGolem = true
        case _: Enemy => hasInsect = true
        case _        => ()
    if !hasInsect then StateStack.push(GolemsWinState)
    if !hasGolem then StateStack.push(InsectsWinState)

}
