package engine

// Un component est rattaché a un GameObject, et lui donne un comportement. Par exemple être afficher, ou se déplacer.
class Component[+G <: GameObject](obj: G) {
  var gameObject: GameObject = obj
  def remove(): Unit = gameObject.removeComponent(this)
  def draw(): Unit = ()
  def loop(): Unit = ()
  def load(): Unit = ()
}
