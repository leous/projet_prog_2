package engine

import scala.compiletime.ops.string
import sfml.window.*
import sfml.system.*
import main.*
import scala.collection.mutable.ListBuffer

class Ressource extends GameObject {

  var amount: Int = 0

  def add(n: Int) =
    amount += n

  def remove(n: Int) =
    amount -= n

  def hasEnough(amountNeeded: Int) =
    (amount >= amountNeeded)

  def size: Int = 1
}

class GroundRessource(file: String) extends Entity(file: String) {

  override val size: Vector2[Float] = Vector2(64, 64)

  val weight: Int = 1

  val ressource: Ressource = Ressource()

  var attachedGolems: List[Golem] = List()

  var wasCarried: Boolean = false
  def isCarried: Boolean =
    attachedGolems.foldLeft(0)(_ + _.carryForce) >= weight

  def canBeCarried(group: ListBuffer[Entity]): Boolean =
    var carriers = this.attachedGolems.filter((g: Golem) => group.contains(g))
    carriers.foldLeft(0)(_ + _.carryForce) >= weight

  def attachGolem(golem: Golem) =
    attachedGolems = golem :: attachedGolems
    golem.isAttached = true
    golem.attachement = this

  def detachAllGolems() =
    attachedGolems.foreach((g: Golem) => g.detach())

  def detachGolemsExcept(group: ListBuffer[Entity]): Unit =
    attachedGolems.foreach((g: Golem) => if !group.contains(g) then g.detach())
  def detachGolemsOnly(group: ListBuffer[Entity]): Unit =
    attachedGolems.foreach((g: Golem) => if group.contains(g) then g.detach())

  override def load() =
    super.load()
    this.addComponent(TargetSelect(this))
    var scale = 0.8 + 0.2 * weight
    this.sprite.scale(Vector2(scale.toFloat, scale.toFloat))

  override def loop() =
    if isCarried then
      var mean: Vector2[Float] = Vector2(0, 0)
      var nbGolems = attachedGolems.length.toFloat
      for (golem <- attachedGolems) do mean = mean + golem.position
      position = (mean + Vector2(10, 20)) * (1 / nbGolems)
      if !wasCarried then
        this.place.sortLayers()
        wasCarried = true
    else
      if wasCarried then this.place.sortLayers()
      wasCarried = false

  override def draw() =
    this.sprite.position = this.position - (this.size * 0.5.toFloat)
    window.draw(this.sprite)

}

object Stone extends Ressource
class GroundStone(file: String = "stone.png")
    extends GroundRessource(file: String) {
  override val weight = 2 + random.nextInt(3)
  override val ressource: Ressource = Stone
}

object Soul extends Ressource
class GroundSoul(file: String = "soul.png")
    extends GroundRessource(file: String) {
  override val ressource: Ressource = Soul
}
