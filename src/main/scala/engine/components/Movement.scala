package engine
import sfml.system.*
import scala.math.*
import selection.Selection.target

//Gère le mouvement, est une ébauche/démo et doit être retravaillé.
class Movement(obj: Entity) extends Component[Entity](obj: Entity) {

  // Calcule un vecteur de direction normalisé
  def directionVector() =
    obj.destination match
      case dest: Vector2[Float] =>
        dest - obj.position
      case target: Entity =>
        target.position - obj.position

  var dir: Vector2[Float] = Vector2(0, 0)

  def updateDir() =
    dir = normalize(directionVector())

  // avance d'un certain pas dans une direction, en fonction de la vitesse de l'entité
  def moveForward(speed: Float) =
    obj.position = obj.position + (dir * speed)

  def cancelMoveForward(speed: Float) =
    obj.position = obj.position - (dir * speed)

  def correctTrajectory(ent: Entity) =
    var collisionDirection = obj.position - ent.position
    collisionDirection = normalize(collisionDirection)
    obj.position = obj.position + (collisionDirection * 3.toFloat)
    ent match
      case ent: Entity with Mobile =>
        ent.position = ent.position - (collisionDirection * 3.toFloat)
      case _ => ()

  def calculateCollisionAndStep() =
    moveForward(obj.speed)
    if isNear() then obj.isMoving = false
    for (entity <- obj.parent.subObjects) do
      entity match
        case entity: Entity =>
          if !entity.isNeutral() && entity != obj && obj.hitbox.intersects(
              entity.hitbox
            )
          then correctTrajectory(entity)
        case _ => ()

  def repulseHitbox() =
    for (entity <- obj.parent.subObjects) do
      entity match
        case entity: Entity =>
          if !entity.isNeutral() && entity != obj && obj.hitbox.intersects(
              entity.hitbox
            )
          then this.dir = this.dir + (obj.position - entity.position)
        case _ => ()

  // vérifie si l'entité est proche de sa destination
  def isNear(): Boolean =
    obj.destination match
      case dest: Vector2[Float] =>
        obj.hitbox.contains(dest)
      // pow(dest.x - obj.position.x, 2) + pow(dest.y - obj.position.y, 2) < 5
      case target: Entity =>
        obj.hitbox.intersects(target.hitbox)

  override def loop() =
    if obj.isMoving then
      calculateCollisionAndStep()
      updateDir()

}

class GolemMovement(obj: Golem) extends Movement(obj: Golem) {

  def increaseCombatPos(maxAmount: Double) =
    if obj.combatPosition == (maxAmount - 1) then obj.combatPosition = 0
    else obj.combatPosition += 1

  def calculateCombatPos(
      center: Vector2[Float],
      radius: Float,
      position: Double
  ) =
    increaseCombatPos(radius)
    var relativePos = Vector2(
      math.cos((obj.combatPosition * math.Pi) / (radius)).toFloat,
      math.sin((obj.combatPosition * math.Pi) / (radius)).toFloat
    )
    center + (relativePos * (radius * 0.5).toFloat)

  def golemCombatMovement() =
    obj.position = calculateCombatPos(
      obj.fightTarget.position,
      obj.fightTarget.size.x,
      obj.combatPosition
    )

  def enterCombat(enemy: Enemy) =
    obj.inCombat = true
    obj.fightTarget = enemy
    obj.combatPosition = math.random() * (enemy.size.x)

  override def calculateCollisionAndStep() =
    moveForward(obj.speed)
    if isNear() then
      obj.destination match
        case enemy: Enemy => enterCombat(enemy)
        case ressource: GroundRessource =>
          if obj.attachement != ressource then ressource.attachGolem(obj)
        case base: Base => if obj.isAttached then base.collect(obj.attachement)
        case _          => ()
      obj.isMoving = false
    else
      for (entity <- obj.parent.subObjects) do
        entity match
          case entity: Entity with Mobile =>
            if !entity.isNeutral() && entity != obj && obj.hitbox.intersects(
                entity.hitbox
              )
            then correctTrajectory(entity)
          case _ => ()

  override def loop() =
    super.loop()
    if obj.fightTarget != obj.destination then obj.inCombat = false
    if obj.inCombat then golemCombatMovement()
}

class DirtBallMovement(obj: DirtBall) extends Movement(obj: Entity) {

  override def load(): Unit =
    super.load()
    updateDir()

  override def loop(): Unit =
    moveForward(obj.speed)
    obj.sprite.rotate(3)
    for (o <- obj.parent.subObjects) do
      o match
        case e: Entity with Health =>
          if obj.hitbox.intersects(e.hitbox) && e != obj.thrower then
            e.health = e.health - obj.damage
            obj.removeThis()
        case _ => ()

}
