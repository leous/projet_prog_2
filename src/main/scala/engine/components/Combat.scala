package engine

import sfml.system.*
import scala.math.abs
import main.*
import gameLoop.GameLoop

//Component gérant le combat. Est une simple ébauche/démo et doit être completement retravaillé,
//notamment les dégats des ennemis

class Hitting(obj: Entity with Combative)
    extends Component[Entity](obj: Entity) {

  def isNear(o: Entity, d: Int) =
    abs(o.position.x - obj.position.x) < (o.size.x + obj.size.x + d) &&
      abs(o.position.y - obj.position.y) < (o.size.y + obj.size.y + d)

  val hitClockAmount: Int =
    1000 / GameLoop.tickRate.toInt // frappe toutes les secondes

  var hitClock: Int = 0

  def clockCycle(): Unit =
    if hitClock <= 0
    then hitClock = obj.attackInterval * hitClockAmount
    else hitClock -= 1

  override def load() =
    hitClock = random.nextInt(obj.attackInterval * hitClockAmount)

  override def loop() =
    clockCycle()

}
class AllyCombat(obj: Golem) extends Hitting(obj: Golem) {

  def hitTargetedEnemy(): Unit = obj.fightTarget match
    case o: Enemy =>
      if obj.hitbox.nearbyHitbox(o.hitbox, 3) && obj.inCombat then
        o.health = o.health - obj.damage
      if o.health <= 0 then obj.inCombat = false
    case _ => ()

  override def loop(): Unit =
    super.loop()
    if hitClock == 0 then hitTargetedEnemy()

}

class EnemyCombat(obj: Enemy with Combative)
    extends Hitting(obj: Enemy with Combative) {

  override def loop(): Unit =
    super.loop()
    if hitClock == 0 then obj.attack()

}
