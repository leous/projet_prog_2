import scala.scalanative.build.*

scalaVersion := "3.2.2"
enablePlugins(ScalaNativePlugin)
enablePlugins(UmlClassDiagramPlugin)

nativeConfig ~= {
  _.withIncrementalCompilation(true)
    .withLTO(LTO.none)
    .withMode(Mode.debug)
}

githubSuppressPublicationWarning := true
githubTokenSource := TokenSource.GitConfig("github.token") || TokenSource
  .Environment("GITHUB_TOKEN")

resolvers += Resolver.githubPackages("lafeychine")
libraryDependencies += "io.github.lafeychine" %%% "scala-native-sfml" % "0.5.1"
classDiagramSettings := classDiagramSettings.value.copy(
  openFolder = true,
  openSvg = true
)
