# Projet Programmation 2

Brasseur Léopold -- Corbard Simon

## Nouveautées

### Base

Les bases sont des entités possèdant des points de vie, elles peuvent donc être attaquées et détruites par les ennemis. Elles peuvent être crées en faisant un clique droit de la souris, à condition que le joueur possède un certain nombre de ressouce pierres. On peut alors faire un clique droit sur une base pour fabriquer un nouveau golem, également sous condition de possèder suffisamment de pierres et d'âme. Ces prix sont configurables dans la classe `Entity`.
### Ressources

Il y a maintenant des ressources qui peuvent être sur le sol : des pierres et des âmes. Les pierres sont deja sur la carte au départ, et peuvent apparaître sur la carte aléatoirement. Les ames sont lachées par les ennemies quand ils meurent. Les ressouces peuvent êtres transportées par les golems, en cliquant dessus une fois les golems selectionnés. Cependant, les ressources ont un certain poid, il faut suffisamment de golems pour les porter. Si ils ne sont pas assez nombreux, ils ne font rien jusqu'à être aidés par d'autres golems. On selectionne les golems porteurs puis la destination pour déplacer la ressource. Si suffisamment de golems sont selectionnés ils la transportent, sinon ils s'en vont sans elle.
Les rochers ont un poid entre 2 et 4 et ont un sprite adéquat à leur poids. Les âmes ont un poid de 1. Il faut transporter les ressources à la base pour les collecter.

### Ennemis

Il y a deux nouveaux ennemis dans le jeu : la sauterelle et le coléoptère en plus du scarabé.

Le scarabé attaque régulièrement un des golems autour de lui.

La sauterelle saute aléatoirement aux alentours et attaque les golems lorsqu'elle saute. Elle ne peut tuer au plus qu'un golem par saut. Elle cherche egalement à attaquer la base. Elle se dirige toujours vers la base si il y en a une, sauf lors d'un combat ou elle saute sur place.

Le coléoptère lance à intervalles trois boules de terre autour de lui, dans des directions aléatoires. Les boules avancent tout droit jusqu'à être trop loin du tireur ou sortir de la carte ou jusqu'à infliger des dégats à une autre entité, qu'elles soit amie ou ennemie.

Tous les combats se déroulent de la même façon : On fait attaquer des golems, qui tournent alors autour de l'ennemi visé en infligeant des dégâts régulièrement. Contrairement à la version précédente, les attaques sont régulières, mais le premier cycle (i.e. temps avant la première frappe) est choisi aléatoirement pour chaque golem au début du jeu.

### Fin de la partie

Si il n'y a plus de golems, le jeu est bloqué et les insectes ont gagnés. Dans le cas inverse, les golems gagnent et un écran de victoire apparaît.

### Editeur

Il est possible de passer en mode editeur avec la touche G.

Dans ce mode plus rien n'est actualisé; on peut modifier la scène et les entités. Il est possible de les déplacer en les glissant, de les supprimer en passant la souris dessus et en appuyant sur backspace et de les faire apparaîtres en appuyant sur la touche du numero associé. Ces informations et les codes des entites sont indiquées dans le mode éditeur.

On peut sauvegarde une scene en faisant ctrl + C + (numero entre 1 et 7).

On peut load une scene en faisant ctrl + V + (numero entre 1 et 7).

Une sauvegarde est representée par un fichier texte, uniquement les données de type, position et santé des entités sont conservée, ainsi que les ressources.


## Structure du moteur de jeu

### Structure principale

Le moteur de jeu est inspiré du moteur Unity. Les différentes scènes du jeu sont composées d'objets (`GameObjects`) auxquels sont rattachés des comportements (`Components`). Les objets possèdent des sous objets, et la scène elle-même est un objet. Le jeu possède deux boucles qui appellent des méthodes uniquement possédées par les `Components` :
- `draw()`, qui gère le rendu de l'objet ou de certaines de ses caractéristiques comme par exemple la santé d`une unité
- `loop()`, qui gère la boucle du jeu, c'est-à-dire les évolutions des objets ou leurs interactions, comme par exemple leur mouvement ou le combat.
Une scène de jeu peut donc être vue comme un arbre dont les nœuds sont des `GameObjects`, et les feuilles des `Components`. Les deux boucles de jeu parcourent alors cet arbre, en exécutant les fonctions associées. 
Pour le moment, les deux boucles s'exécutent toutes les 50ms, mais à terme la boucle de `draw()` n'aura pas de contraintes de temps, elle s'exécute en permanence, et la boucle de `loop()` toutes les 50ms, dans deux threads différents.
Lors de l'ouverture d'une scène, celle-ci est également parcourue par une fonction qui exécute la méthode load() sur les `GameObjects`. Cette fonctionnalité pourrait disparaître au profit d'un code mieux structuré et plus performant, notamment lors de la création d'instance d'objets.

### Les boucles de jeu

Une boucle jeu se déroule en trois étapes :
 - Gestion des événements : ils sont tous gérés dans la fonction `handleEvents`.
 - Boucle de jeu : le jeu est mis à jour grâce à la pile d'états, pour ralentir cette étape on fait en sorte qu'elle ne s'exécute au plus que toutes les 20 millisecondes, si elle est appelée trop tôt elle est donc passée.
 - Dessin du jeu : le jeu est dessiné grâce à la pile d'états.

### Gestion des `scenes`

Les scènes sont les objets principaux des états du jeu, c'est elles qui contiennent tous les autres objets, entitées, ressources, ... et qui sont affichées à l'écran. Elles ont un attribut view qui permet de dessiner la scène par rapport à sa propre vue, par exemple on veut avoir le contrôle de la vue sur la scène de jeu mais pas sur l'inventaire.

### Gestion des `events`

Les événements sont gérés par l'objet `Events`, à chaque passage dans la boucle principale des attributs de l'objet sont mis à jour. ceux liés au clavier ou aux boutons de la souris mettent à jour une map. Cette map contient toutes les informations sur les touches qui sont pressées et est accessible à n'importe quel moment depuis l'extérieur.


### Gestion des états du jeu

Un état du jeu ou `GameState` est l'ensemble des données et des visuels d'une étape du jeu. Par exemple, le menu de démarrage, le menu pause, une cinématique ou simplement les phases de jeu sont des états différents du jeu.

Les états ont une méthode `updateState` qui leur permet d'interagir avec la pile, par exemple appuyer sur échap permet de push le menu pause sur le dessus de la pile.

Les états ont un attribut `layers` qui est en fait une pile de scènes, cette pile n'est jamais vide et permet d'afficher les différentes couches de cet état, par exemple le jeu, l'inventaire par dessus.

### La pile d'états

Pour mieux faire interagir ces états entre eux on implémente un objet `StateStack`. C'est une pile des états du jeu, elle donne toujours la priorité à l`état sur le dessus.

Pour les boucles de jeu, on actualise la pile :
 - Si la pile est vide, le jeu se termine et la fenêtre est fermée.
 - Sinon elle fait jouer une boucle à l`état sur le dessus de la pile et on actualise cet état.
 
Pour le dessin du jeu, on dessine les états dans l`ordre :
 - certains états ont l'attribut `isTransparent` à `true`, dans ce cas on dessine d'abord les états en dessous (par exemple on veut afficher le jeu derrière le menu pause), puis on le dessine.
 - si un état n'est pas transparent (la quasi totalité des états) on le dessine et on s’arrête.

## Description des `GameObjects`

### Entités

Les entités sont les éléments principaux de la scène. Elles sont presque toutes affichées à l'écran, et c'est avec elles que le joueur peut interagir. Elles possèdent donc plusieurs champs, comme une `position` ou une `size`.
Pour éviter de charger la texture d'une entité à chaque instance d'entité, on utilisera une `Map` dans l'objet `EntityTable` qui permettra au sprite associé à l'entité d'accéder à la texture une fois chargée.

### Unités

Il existe pour le moment deux types d'unités, les `Golems`, qui sont contrôlés par le joueur, et les unités `Enemy1` que le joueur doit combattre.

#### Mouvements (`Movement.scala`, `EntitySelection.scala`)

Le joueur peut seulement déplacer les golems seulement. Il doit les sélectionner, soit en cliquant dessus, soit avec shit + clic ce qui forme une zone carrée à l'écran et sélectionne ceux qui sont dedans. On peut alors les déplacer n'importe où. De plus, les entités ont une physique de collision, elles se repoussent losqu'elles se recontrent. Leurs hitbox sont rondes.

Si le joueur sélectionne un ennemi, une fois que les golems l'ont atteint, il vont se mettre a l'attaquer en lui tournant autour.

#### Combat (`Hitting.scala`, `HealthManagement.scala`)

Lorsqu'un golem attaque un ennemi, il lui enlève 1 point de vie toutes les 5 secondes.
L'ennemi va tuer un golem qui l'attaque à chaque intervalle de temps, en fonction de sa puissance.

### Ressources (`Ressources.scala`, `Base.scala`)

Les ressources peuvent être transportées jusqu'à la base, puis collectées. Elles peuvent alors être utilisées pour produire des golems.

### Schéma résumant les classes du moteur

![classes](readme_img/projprog2-classDiagram.svg)

## Éléments enlevés

### Carte de tuiles (`TileMap`)

Une des premières implémentations à été l'écriture d'un parseur qui lisait des fichiers textes, puis générait une grille de tuiles, construisant ainsi une carte. Il a été décidé de le supprimer car celui-ci générait un sprite pour chaque tuile, ce qui demandait beaucoup de ressources. Un autre système sera implémenté pour gérer les backgrounds (voir la section suivante)

## Eléments prévus non spécifiés dans le sujet

### Générateur de scène

Il est prévu d'écrire un générateur de scène à partir de fichiers json ou yaml, qui seront parsés. Ces fichiers contiendront une liste des entités présentes au départ, et leur caractéristiques, ainsi qu'une description des éléments de décors.