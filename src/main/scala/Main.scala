package main

import sfml.graphics.*
import sfml.system.*
import sfml.window.*
import engine.*
import scala.collection.mutable.ListBuffer
import scenes.*
import events.*
import gameLoop.*

import sfml.window.Mouse.Button.Left

val window = RenderWindow(VideoMode(1024, 768), "ProjProg2")

val font = {
  var f = Font()
  f.loadFromFile("src/main/resources/Minimal5x7.ttf")
  f
}

val random = new scala.util.Random

@main def main =

  EntityTable.load()

  StateStack.push(GamePlayGameState(loadScene(0)))

  while window.isOpen() do
    window.clear()
    Events.handleWindowEvents()
    GameLoop.gameUpdate()
    window.display()
