package scenes

import main.*
import sfml.graphics.*
import sfml.system.*
import sfml.window.*
import sfml.window.Mouse.Button.*
import engine.*
import java.io.File
import sfml.Immutable
import selection.*
import events.*

//localisation des fonds pour les scenes
val pathScene: String = "src/main/resources/scenesBackground/"

//une scene ne dépends que de son fond, donné par le nom du fichier .png associé
class Scene(file: String) extends GameObject {

  val texture: Texture = Texture()
  var sprite: Sprite = _

  override def draw(): Unit =
    window.view = Immutable(this.view)
    window.draw(this.sprite)

  // la view doit etre changée en fonction de la scène
  var view: View = _

  var loaded: Boolean = false

  override def load(): Unit =
    super.load()
    texture.loadFromFile(pathScene + file)
    sprite = Sprite(texture)
    loaded = true

}

//scenes dans lesquelles on a le controle de la caméra et dans laquelle on peut ajouter des entitées
class GamePlayScene(map: String) extends Scene(map: String) {

  override def load(): Unit =
    super.load()
    addSubObject(Selection)
    this.view = View((450, 450), (800, 800 * window.size.y / window.size.x))
    addComponent(CameraMovement(this))
    addComponent(TargetSelect(this))

  def addEntity[E <: Entity](ent: E, x: Float, y: Float) =
    ent.position = Vector2(x, y)
    ent.place = this
    ent.load()
    for (comp <- ent.components) do comp.load()
    addSubObject(ent)
    sortLayers()

  // spwan but check the cost and consume ressources
  def spawnEntity(ent: Entity, x: Float, y: Float): Unit =
    if Soul.hasEnough(ent.soulCost) && Stone.hasEnough(ent.stoneCost) then
      Soul.remove(ent.soulCost)
      Stone.remove(ent.stoneCost)
      this.addEntity(ent, x, y)

  def sortLayers(): Unit =
    def sortFunction(o1: GameObject, o2: GameObject): Boolean =
      (o1, o2) match
        case (_: Base, _)            => true
        case (_, _: Base)            => false
        case (_, Selection)          => true
        case (Selection, _)          => false
        case (_, r: GroundRessource) => r.isCarried
        case (r: GroundRessource, _) => !r.isCarried
        case (_, _)                  => true
    subObjects = subObjects.sortWith(sortFunction)

  override def loop() =
    super.loop()
    if Events.isReleased(Right) then
      this.spawnEntity(Base(), Events.mousePos.x, Events.mousePos.y)

}
