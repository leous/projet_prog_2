package engine

import sfml.window.*
import selection.*
import events.*
import sfml.window.Mouse.Button.Left
import sfml.system.*
import scala.math.*
import scenes.*

def isOver(ent: Entity, pos: Vector2[Float]): Boolean =
  ent.sprite.globalBounds.contains(pos)

//Selection des entites. Comportement sujet à changements.
class AllySelect(obj: Entity) extends Component[Entity](obj: Entity) {

  def isSelectionOver() =
    Selection.zoneSelected.intersects(obj.sprite.globalBounds)

  override def loop() =
    if !Selection.isLongSelecting && Selection.wasLongSelecting &&
      this.isSelectionOver()
      || !Selection.isSelecting && Selection.wasSelecting &&
      isOver(obj, Events.mousePos) && isOver(obj, Selection.firstClicPos)
    then Selection.addSelection(obj)

}

class TargetSelect(obj: GameObject)
    extends Component[GameObject](obj: GameObject) {

  override def loop() =
    obj match
      case res: GroundRessource =>
        if !Selection.isSelecting && Selection.wasSelecting &&
          isOver(res, Events.mousePos) && isOver(res, Selection.firstClicPos)
          // bug : faire en sorte de ne pas target une ressource en esseyant de selectioner ses porteurs
        then Selection.target(res)
      case ent: Entity =>
        if !Selection.isSelecting && Selection.wasSelecting &&
          isOver(ent, Events.mousePos) && isOver(ent, Selection.firstClicPos)
        then Selection.target(ent)
      case scene: Scene =>
        var targetable: Boolean = true
        for (subObj <- scene.subObjects) do
          subObj match
            case ent: Entity =>
              if isOver(ent, Events.mousePos) then targetable = false
            case _ => ()
        if !Selection.isSelecting && Selection.wasSelecting && targetable &&
          dist(Events.mousePos, Selection.firstClicPos) < 4
        then Selection.target(Events.mousePos)
      case _ => ()

}
