package scenes

import main.*
import sfml.graphics.*
import sfml.system.*
import sfml.window.*
import engine.*
import java.io.File
import sfml.Immutable
import selection.*
import events.*

//scene de l'ecran de win des insects
object MenuPauseScene extends Scene("infoScreen.png") {

  val winText = {
    val text = Text()
    text.font = font
    text.string = "    PAUSE    \nPress [ESCAPE]"
    text.characterSize = 38
    text.fillColor = Color.White()
    text
  }

  this.view = View((128, 64), (400, 400 * window.size.y / window.size.x))

  override def load() =
    super.load()
    winText.position = Vector2(45, 20)

  override def draw() =
    super.draw()
    window.draw(winText)

}

//scene de l'ecran de win des insects
object InsectsWinScene extends Scene("infoScreen.png") {

  val winText = {
    val text = Text()
    text.font = font
    text.string = "Insects   WIN !"
    text.characterSize = 50
    text.fillColor = Color.White()
    text
  }

  this.view = View((128, 64), (400, 400 * window.size.y / window.size.x))

  override def load() =
    super.load()
    winText.position = Vector2(23, 25)

  override def draw() =
    super.draw()
    window.draw(winText)

}

object GolemsWinScene extends Scene("infoScreen.png") {

  val winText = {
    val text = Text()
    text.font = font
    text.string = "Golems   WIN !"
    text.characterSize = 50
    text.fillColor = Color.White()
    text
  }

  this.view = View((128, 64), (400, 400 * window.size.y / window.size.x))

  override def load() =
    super.load()
    winText.position = Vector2(33, 25)

  override def draw() =
    super.draw()
    window.draw(winText)

}
