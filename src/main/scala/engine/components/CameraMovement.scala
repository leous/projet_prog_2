package engine

import scala.math.sqrt
import scenes.*
import main.*
import events.*
import sfml.window.Keyboard.Key.*
import engine.*
import sfml.graphics.*
import sfml.system.*

//gere le deplacement de la camera, specialement pour une scene de jeu
class CameraMovement(scene: Scene) extends Component[Scene](scene: Scene) {

  val speed: Float = 20

  // Renvoie un déplacement qui ne sort pas view de la map
  // !!! attention si la view est déja en dehors de la map, la camera peut etre bloquée
  def notEscape(move: Vector2[Float]): Vector2[Float] =
    var trueMove: Vector2[Float] = move
    if !scene.sprite.localBounds.contains(
        scene.view.center - (scene.view.size.x * 0.5.toFloat, 0.toFloat) + move
      ) ||
      !scene.sprite.localBounds.contains(
        scene.view.center + (scene.view.size.x * 0.5.toFloat, 0.toFloat) + move
      )
    then trueMove = (0.toFloat, trueMove.y)
    if !scene.sprite.localBounds.contains(
        scene.view.center - (0.toFloat, scene.view.size.y * 0.5.toFloat) + move
      ) ||
      !scene.sprite.localBounds.contains(
        scene.view.center + (0.toFloat, scene.view.size.y * 0.5.toFloat) + move
      )
    then trueMove = (trueMove.x, 0.toFloat)
    return trueMove

  // normalise le déplacement sur la norme de la vitesse
  def setSpeed(vect: Vector2[Float]) =
    if vect.x == 0 && vect.y == 0 then vect
    else vect * (speed / sqrt(vect.x * vect.x + vect.y * vect.y).toFloat)

  // régait aux touches Z Q S D pour déplacer la caméra
  override def loop(): Unit =
    var move: Vector2[Float] = (0, 0)
    if Events.isPressed(KeyZ) then move += (0, -1)
    if Events.isPressed(KeyS) then move += (0, 1)
    if Events.isPressed(KeyQ) then move += (-1, 0)
    if Events.isPressed(KeyD) then move += (1, 0)
    scene.view.move((notEscape(setSpeed(move))))

}
