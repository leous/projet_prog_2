package gameLoop

import engine.*
import main.*
import events.*

//Fonctions permettant de dessiner ou actualiser recursivement un GameObject

def draw[G <: GameObject, C <: Component[GameObject]](drawable: G | C): Unit =
  drawable match
    case obj: GameObject =>
      obj.draw()
      obj.subObjects.foreach(draw)
      obj.components.foreach(draw)
    case comp: Component[GameObject] =>
      comp.draw()

def loop[G <: GameObject, C <: Component[GameObject]](loopable: G | C): Unit =
  loopable match
    case obj: GameObject =>
      obj.subObjects.reverseIterator.foreach(loop)
      obj.components.foreach(loop)
      obj.loop()
    case comp: Component[GameObject] =>
      comp.loop()

object GameLoop {

  val tickRate: Long = 20 // temps (en ms) entre deux loop
  var prevUpdate: Long = System.currentTimeMillis // date de la derniere loop

  // methode qui met a jour le jeu en mettant a jour la pile
  def gameUpdate(): Unit = StateStack.stack match
    case List() =>
      window.close() // le jeu s'arrete si la pile est vide lors d'une update
    case (state: GameState) :: _ =>
      if System.currentTimeMillis - prevUpdate > tickRate
      then // l'etat du jeu est mis a jour toutes les "tickRate" ms
        prevUpdate = System.currentTimeMillis
        state.loopState()
        state.updateState()
        Events.reset()
      StateStack.drawStack()
}
