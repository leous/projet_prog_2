package engine

import engine.*
import sfml.system.Vector2
import main.*

class Sauterelle(file: String = "sauterelle.png") extends Enemy(file: String) {

  var combatAnimationStep = 0
  var jumpStartPos = this.position
  var jumpEndPos = this.position

  override def load() =
    super.load()
    maxHealth = 7
    health = maxHealth
    attackInterval = 5
    damage = 1

  override def draw() =
    // very not pretty but there is only two animation to see when the enemy attack so...
    if combatAnimationStep > 0 then
      this.position =
        jumpStartPos * (1 - combatAnimationStep.toFloat / 211.toFloat) + jumpEndPos * (combatAnimationStep.toFloat / 211.toFloat)
      combatAnimationStep match
        case 1   => this.sprite.scale(0.9, 0.9)
        case 31  => this.sprite.scale(1.1, 1.1)
        case 61  => this.sprite.scale(1.2, 1.2)
        case 91  => this.sprite.scale(1.2, 1.2)
        case 121 => this.sprite.scale(1.1, 1.1)
        case 151 => this.sprite.scale(0.9, 0.9)
        case 181 => this.sprite.scale(0.9, 0.9)
        case 211 => {
          this.sprite.scale(0.788, 0.788); combatAnimationStep = -1
        }
        case _ => ()
      combatAnimationStep += 1
    super.draw()

  def hitArea(): Unit =
    var hasHit = false
    for (o <- this.parent.subObjects) do
      o match
        case o: Golem =>
          if this.hitbox.nearby(o.position, 20) && !hasHit then
            o.health = o.health - this.damage
            hasHit = true
        case b: Base =>
          if this.hitbox.nearby(b.position, 20) then
            b.health = b.health - this.damage
        case _ => ()

  def findTarget(): Boolean =
    var minDist = 1000000000.toFloat
    var found = false
    for (o <- this.parent.subObjects) do
      o match
        case b: Base =>
          if dist(b.position, this.position) < minDist then
            minDist = dist(b.position, this.position)
            fightTarget = b
            found = true
        case _ => ()
    return found

  override def attack(): Unit =
    hitArea()
    jumpStartPos = this.position
    if !inCombat then
      if findTarget() then
        if dist(this.position, this.fightTarget.position) < 200 then
          jumpEndPos = this.fightTarget.position + Vector2(10, 10)
        else
          jumpEndPos = this.position + normalize(
            (this.fightTarget.position - this.position)
          ) * 200 + Vector2(10, 10)
      else
        jumpEndPos = this.position + Vector2(
          100 - random.nextInt(200),
          100 - random.nextInt(200)
        )
    else jumpEndPos = this.position
    this.sprite.rotation = random.nextInt(360)
    combatAnimationStep = 1

}
