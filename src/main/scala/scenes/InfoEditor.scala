package scenes

import sfml.graphics.*
import sfml.system.*
import main.*
import selection.*
import engine.*
import sfml.Immutable

object InfoEditor extends Scene("") {

  val info = {
    val text = Text()
    text.font = font
    text.string =
      "EDITOR mode :\n - [CTRL-C] + 1,2,...,7 to save map\n - [CTRL-V] + 1,2,...,7 to load map\n - [BackSpace] over an entity to remove\n - drag the mouse to place entity\n - code to spawn :\n   [1] - Base\n   [2] - Golem\n   [3] - Scarabe\n   [4] - Sauterelle\n   [5] - Coleoptere\n   [6] - Stone\n   [7] - Soul"
    text.characterSize = 30
    text.fillColor = Color.White()
    text
  }

  this.view = View(
    (350, 351 * window.size.y / window.size.x),
    (700, 700 * window.size.y / window.size.x)
  )

  override def draw(): Unit =
    window.view = Immutable(this.view)
    window.draw(info)

  override def load(): Unit =
    info.position = Vector2(12, 0)

}
