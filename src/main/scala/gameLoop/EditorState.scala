package gameLoop

import main.*
import scenes.*
import engine.*
import sfml.graphics.*
import events.*
import sfml.window.Keyboard.Key.*
import sfml.window.Mouse.Button.*
import scala.collection.mutable.Stack

class EditorState(initialScene: Scene) extends GameState(initialScene: Scene) {

  override def load() =
    super.load()
    InfoEditor.load()
    this.layers.push(InfoEditor)

  override def loopState(): Unit =
    this.mainScene.components.foreach(loop)

  override def updateState(): Unit =
    if Events.isReleased(KeyG) then
      StateStack.pop()
      StateStack.push(GamePlayGameState(this.mainScene))
    for (o <- this.mainScene.subObjects) do
      o match
        case e: Entity =>
          if e.hitbox.contains(Events.mousePos) then
            if Events.isPressed(Left) then e.position = Events.mousePos
            if Events.isPressed(KeyBackspace) then e.removeThis()
            (e match
              case ent: Entity with Combative =>
                ent.inCombat = false
              case _ => ()
            )
        case _ => ()
    this.mainScene match
      // very not pretty
      case place: GamePlayScene =>
        if Events.isPressed(KeyLControl) && Events.isPressed(KeyC) then
          if Events.isReleased(KeyNum1) then saveScene(mainScene, 1)
          if Events.isReleased(KeyNum2) then saveScene(mainScene, 2)
          if Events.isReleased(KeyNum3) then saveScene(mainScene, 3)
          if Events.isReleased(KeyQuote) then saveScene(mainScene, 4)
          if Events.isReleased(KeyNum5) then saveScene(mainScene, 5)
          if Events.isReleased(KeyHyphen) then saveScene(mainScene, 6)
          if Events.isReleased(KeyNum7) then saveScene(mainScene, 7)
        else if Events.isPressed(KeyLControl) && Events.isPressed(KeyV) then
          if Events.isReleased(KeyNum1) then {
            StateStack.pop(); StateStack.push(EditorState(loadScene(1)))
          }
          if Events.isReleased(KeyNum2) then {
            StateStack.pop(); StateStack.push(EditorState(loadScene(2)))
          }
          if Events.isReleased(KeyNum3) then {
            StateStack.pop(); StateStack.push(EditorState(loadScene(3)))
          }
          if Events.isReleased(KeyQuote) then {
            StateStack.pop(); StateStack.push(EditorState(loadScene(4)))
          }
          if Events.isReleased(KeyNum5) then {
            StateStack.pop(); StateStack.push(EditorState(loadScene(5)))
          }
          if Events.isReleased(KeyHyphen) then {
            StateStack.pop(); StateStack.push(EditorState(loadScene(6)))
          }
          if Events.isReleased(KeyNum7) then {
            StateStack.pop(); StateStack.push(EditorState(loadScene(7)))
          }
        else
          if Events.isReleased(KeyNum1) then
            place.addEntity(Base(), Events.mousePos.x, Events.mousePos.y)
          if Events.isReleased(KeyNum2) then
            place.addEntity(Golem(), Events.mousePos.x, Events.mousePos.y)
          if Events.isReleased(KeyNum3) then
            place.addEntity(Scarabe(), Events.mousePos.x, Events.mousePos.y)
          if Events.isReleased(KeyQuote) then
            place.addEntity(Sauterelle(), Events.mousePos.x, Events.mousePos.y)
          if Events.isReleased(KeyNum5) then
            place.addEntity(Coleo(), Events.mousePos.x, Events.mousePos.y)
          if Events.isReleased(KeyHyphen) then
            place.addEntity(GroundStone(), Events.mousePos.x, Events.mousePos.y)
          if Events.isReleased(KeyNum7) then
            place.addEntity(GroundSoul(), Events.mousePos.x, Events.mousePos.y)
      case _ => ()

}
