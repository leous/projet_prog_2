package engine

import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.ListBuffer
import sfml.system.*
import sfml.graphics.*

// Les GameObjects sont les briques de base du jeu: Ils constituent les éléments d'une scène,
// par exemple les enemis, les alliés, les ressources, mais aussi des éléments de décors
class GameObject {
  // Ils possèdent des sous objets qui leurs sont attachés
  var subObjects: Vector[GameObject] = Vector()

  // Et différents comportements, ou components
  var components: ListBuffer[Component[GameObject]] = ListBuffer()

  // On a également besoin d'avoir un pointeur vers l'objet parent
  var parent: GameObject = null

  def addSubObject[T <: GameObject](obj: T) =
    obj.parent = this
    subObjects = obj +: subObjects
  def removeSubObject[T <: GameObject](obj: T) =
    subObjects = subObjects.filterNot(obj.==)

  def removeComponent[T <: Component[GameObject]](comp: T) =
    components -= comp
  def addComponent[T <: Component[GameObject]](comp: T) =
    comp.gameObject = this
    components += comp

  def removeThis() =
    this.parent.removeSubObject(this)

  def load(): Unit = ()
  def draw(): Unit = ()
  def loop(): Unit = ()
}
