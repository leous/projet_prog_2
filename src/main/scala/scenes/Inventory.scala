package scenes

import sfml.graphics.*
import sfml.system.*
import main.*
import selection.*
import engine.*

object Inventory extends Scene("inventory.png") {

  val stoneDisplay = {
    val text = Text()
    text.font = font
    text.string = "0"
    text.characterSize = 35
    text.fillColor = Color.White()
    text
  }

  val soulDisplay = {
    val text = Text()
    text.font = font
    text.string = "0"
    text.characterSize = 35
    text.fillColor = Color.White()
    text
  }

  this.view = View(
    (350, 351 * window.size.y / window.size.x),
    (700, 700 * window.size.y / window.size.x)
  )

  override def draw(): Unit =
    super.draw()
    window.draw(stoneDisplay)
    window.draw(soulDisplay)

  override def loop(): Unit =
    super.loop()
    stoneDisplay.string = Stone.amount.toString
    soulDisplay.string = Soul.amount.toString

  override def load(): Unit =
    // this.addSubObject(Selection)
    super.load()
    stoneDisplay.position = Vector2(50, -8)
    soulDisplay.position = Vector2(125, -8)

}
