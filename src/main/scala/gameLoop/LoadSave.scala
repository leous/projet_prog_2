package gameLoop

import main.*
import scenes.*
import engine.*
import sfml.graphics.*
import events.*
import sfml.window.Keyboard.Key.*
import sfml.window.Mouse.Button.*
import scala.collection.mutable.Stack
import java.io.*
import scala.io.Source

val savePath: String = "src/main/resources/saves/save" // + 1 , 2 , ... ou 7

def saveScene(scene: Scene, save: Int): Unit =
  val saveFile = savePath + save.toString
  val pw = new PrintWriter(new File(saveFile))
  pw.write(Stone.amount.toString + "," + Soul.amount.toString + "\n")
  for (o <- scene.subObjects) do
    var saveObj = true
    o match
      case e: Entity =>
        e match
          case _: Base        => pw.write("1,")
          case _: Golem       => pw.write("2,")
          case _: Scarabe     => pw.write("3,")
          case _: Sauterelle  => pw.write("4,")
          case _: Coleo       => pw.write("5,")
          case _: GroundStone => pw.write("6,")
          case _: GroundSoul  => pw.write("7,")
          case _              => saveObj = false
        if saveObj then
          pw.write(
            e.position.x.toInt.toString + "," + e.position.y.toInt.toString
          )
          e match
            case ent: Entity with Health =>
              pw.write("," + ent.health.toString)
            case _ => ()
          pw.write("\n")
      case _ => ()
  pw.close()
  print("game saved\n")

def loadScene(save: Int): Scene =
  val saveFile = savePath + save.toString
  val new_scene = GamePlayScene("jardin.png")
  var hasLoadRes = false
  for (line <- Source.fromFile(saveFile).getLines) do
    var parsed = line.split(",")
    if hasLoadRes then
      var ent: Entity = {
        if parsed(0) == "1" then Base()
        else if parsed(0) == "2" then Golem()
        else if parsed(0) == "3" then Scarabe()
        else if parsed(0) == "4" then Sauterelle()
        else if parsed(0) == "5" then Coleo()
        else if parsed(0) == "6" then GroundStone()
        else GroundSoul()
      }
      new_scene.addEntity(ent, parsed(1).toInt, parsed(2).toFloat)
      ent match
        case e: Entity with Health => e.health = parsed(3).toInt
        case _                     => ()
    else
      Stone.amount = parsed(0).toInt
      Soul.amount = parsed(1).toInt
      hasLoadRes = true
  print("game loaded\n")
  new_scene
